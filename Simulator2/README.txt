Introduction:
This simulator gives a more indepth look at the actual simulation process in the computer architecture.

Note: 
1. A file entitled ?ucode3? which holds the control store. 
2. A file entitled ?shuffle? which is an assembled LC-3b program. 

The Shell: The purpose of the shell is to provide the user with commands to control the execution of the simulator. The shell accepts one or more ISA programs as arguments and loads them into the memory image. In order to extract information from the simulator, a file named dumpsim will be created to hold information requested from the simulator. The shell supports the following 
commands: 
 
1. go � simulate the program until a HALT instruction is executed. 
2. run <n> � simulate the execution of the machine for n instructions 
3. mdump <low> <high> � dump the contents of memory, from location ?low? to location high? to the screen and the dump file 
4. rdump � dump the current instruction count, the contents of R0�R7, PC, and condition codes to the screen and the dump file. 
5. ? � print out a list of all shell commands. 
6. quit � quit the shell 


The simulation routines:
The simulation routines carry out the cycle-by-cycle simulation of the input LC-3b program. The simulation of any cycle is based on the contents of the current latches in the system. The simulation consists of two concurrently executing phases: 
 
The microsequencer phase uses 9 bits from the microinstruction register and appropriate literals from the datapath to determine the next microinstruction. Function ?eval_micro_sequencer evaluates this phase. 
 
The datapath phase uses 26 bits of the microinstruction to manipulate the data in the datapath. Each microinstruction must be literally interpreted. For example, if the ?GateMDR? bit is asserted, then data must go from the MDR onto the bus. You must also establish an order for events to occur during a machine cycle. For example, data should be gated onto the bus first, and loaded into a register at the end of the cycle. Simulate these events by writing the code for functions eval_bus_drivers?, ?drive_bus? and ?latch_datapath_values?. 
We will assume a memory operation takes five cycles to complete. That is, the ready bit is asserted at the end of the fourth cycle. Function ?cycle_memory? emulates memory.

Example:
Assembly file
   .ORIG x3000
   AND R0, R0, #0
   HALT
   .END
Object file
0x3000
0x5020
0xF025
Dumpsim output after running each cycle for the AND instruction
This dumpsim output was obtained by repeating the commands run 1 and rdump 9 times.
Current register/bus values :
-------------------------------------
Cycle Count  : 1
PC           : 0x3002
IR           : 0x0000
STATE_NUMBER : 0x0021

BUS          : 0x3000
MDR          : 0x0000
MAR          : 0x3000
CCs: N = 0  Z = 1  P = 0
Registers:
0: 0x0000
1: 0x0000
2: 0x0000
3: 0x0000
4: 0x0000
5: 0x0000
6: 0x0000
7: 0x0000


Current register/bus values :
-------------------------------------
Cycle Count  : 2
PC           : 0x3002
IR           : 0x0000
STATE_NUMBER : 0x0021

BUS          : 0x0000
MDR          : 0x0000
MAR          : 0x3000
CCs: N = 0  Z = 1  P = 0
Registers:
0: 0x0000
1: 0x0000
2: 0x0000
3: 0x0000
4: 0x0000
5: 0x0000
6: 0x0000
7: 0x0000


Current register/bus values :
-------------------------------------
Cycle Count  : 3
PC           : 0x3002
IR           : 0x0000
STATE_NUMBER : 0x0021

BUS          : 0x0000
MDR          : 0x0000
MAR          : 0x3000
CCs: N = 0  Z = 1  P = 0
Registers:
0: 0x0000
1: 0x0000
2: 0x0000
3: 0x0000
4: 0x0000
5: 0x0000
6: 0x0000
7: 0x0000


Current register/bus values :
-------------------------------------
Cycle Count  : 4
PC           : 0x3002
IR           : 0x0000
STATE_NUMBER : 0x0021

BUS          : 0x0000
MDR          : 0x0000
MAR          : 0x3000
CCs: N = 0  Z = 1  P = 0
Registers:
0: 0x0000
1: 0x0000
2: 0x0000
3: 0x0000
4: 0x0000
5: 0x0000
6: 0x0000
7: 0x0000


Current register/bus values :
-------------------------------------
Cycle Count  : 5
PC           : 0x3002
IR           : 0x0000
STATE_NUMBER : 0x0021

BUS          : 0x0000
MDR          : 0x0000
MAR          : 0x3000
CCs: N = 0  Z = 1  P = 0
Registers:
0: 0x0000
1: 0x0000
2: 0x0000
3: 0x0000
4: 0x0000
5: 0x0000
6: 0x0000
7: 0x0000


Current register/bus values :
-------------------------------------
Cycle Count  : 6
PC           : 0x3002
IR           : 0x0000
STATE_NUMBER : 0x0023

BUS          : 0x0000
MDR          : 0x5020
MAR          : 0x3000
CCs: N = 0  Z = 1  P = 0
Registers:
0: 0x0000
1: 0x0000
2: 0x0000
3: 0x0000
4: 0x0000
5: 0x0000
6: 0x0000
7: 0x0000


Current register/bus values :
-------------------------------------
Cycle Count  : 7
PC           : 0x3002
IR           : 0x5020
STATE_NUMBER : 0x0020

BUS          : 0x5020
MDR          : 0x5020
MAR          : 0x3000
CCs: N = 0  Z = 1  P = 0
Registers:
0: 0x0000
1: 0x0000
2: 0x0000
3: 0x0000
4: 0x0000
5: 0x0000
6: 0x0000
7: 0x0000


Current register/bus values :
-------------------------------------
Cycle Count  : 8
PC           : 0x3002
IR           : 0x5020
STATE_NUMBER : 0x0005

BUS          : 0x0000
MDR          : 0x5020
MAR          : 0x3000
CCs: N = 0  Z = 1  P = 0
Registers:
0: 0x0000
1: 0x0000
2: 0x0000
3: 0x0000
4: 0x0000
5: 0x0000
6: 0x0000
7: 0x0000


Current register/bus values :
-------------------------------------
Cycle Count  : 9
PC           : 0x3002
IR           : 0x5020
STATE_NUMBER : 0x0012

BUS          : 0x0000
MDR          : 0x5020
MAR          : 0x3000
CCs: N = 0  Z = 1  P = 0
Registers:
0: 0x0000
1: 0x0000
2: 0x0000
3: 0x0000
4: 0x0000
5: 0x0000
6: 0x0000
7: 0x0000