/*
 * Name :  Qian Wen
*/

/***************************************************************/
/*                                                             */
/*   LC-3b Instruction Level Simulator                         */
/*                                                             */
/*   EE 460N                                                   */
/*   The University of Texas at Austin                         */
/*                                                             */
/***************************************************************/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/***************************************************************/
/*                                                             */
/* Files: shuffle   LC-3b machine language program file     */
/*                                                             */
/***************************************************************/

/***************************************************************/
/* These are the functions you'll have to write.               */
/***************************************************************/

void process_instruction();

/***************************************************************/
/* A couple of useful definitions.                             */
/***************************************************************/
#define FALSE 0
#define TRUE  1

/***************************************************************/
/* Use this to avoid overflowing 16 bits on the bus.           */
/***************************************************************/
#define Low16bits(x) ((x) & 0xFFFF)

/***************************************************************/
/* Main memory.                                                */
/***************************************************************/
/* MEMORY[A][0] stores the least significant byte of word at word address A
  MEMORY[A][1] stores the most significant byte of word at word address A
*/

#define WORDS_IN_MEM    0x08000
int MEMORY[WORDS_IN_MEM][2];

/***************************************************************/

/***************************************************************/

/***************************************************************/
/* LC-3b State info.                                           */
/***************************************************************/
#define LC_3b_REGS 8

int RUN_BIT;        /* run bit */


typedef struct System_Latches_Struct{

    int PC,                /* program counter */
            N,                /* n condition bit */
            Z,                /* z condition bit */
            P;                /* p condition bit */
    int REGS[LC_3b_REGS]; /* register file. */
} System_Latches;

/* Data Structure for Latch */

System_Latches CURRENT_LATCHES, NEXT_LATCHES;

/***************************************************************/
/* A cycle counter.                                            */
/***************************************************************/
int INSTRUCTION_COUNT;

/***************************************************************/
/*                                                             */
/* Procedure : help                                            */
/*                                                             */
/* Purpose   : Print out a list of commands                    */
/*                                                             */
/***************************************************************/
void help() {
    printf("----------------LC-3b ISIM Help-----------------------\n");
    printf("go               -  run program to completion         \n");
    printf("run n            -  execute program for n instructions\n");
    printf("mdump low high   -  dump memory from low to high      \n");
    printf("rdump            -  dump the register & bus values    \n");
    printf("?                -  display this help menu            \n");
    printf("quit             -  exit the program                  \n\n");
}

/***************************************************************/
/*                                                             */
/* Procedure : cycle                                           */
/*                                                             */
/* Purpose   : Execute a cycle                                 */
/*                                                             */
/***************************************************************/
void cycle() {

    process_instruction();
    CURRENT_LATCHES = NEXT_LATCHES;
    INSTRUCTION_COUNT++;
}

/***************************************************************/
/*                                                             */
/* Procedure : run n                                           */
/*                                                             */
/* Purpose   : Simulate the LC-3b for n cycles                 */
/*                                                             */
/***************************************************************/
void run(int num_cycles) {
    int i;

    if (RUN_BIT == FALSE) {
        printf("Can't simulate, Simulator is halted\n\n");
        return;
    }

    printf("Simulating for %d cycles...\n\n", num_cycles);
    for (i = 0; i < num_cycles; i++) {
        if (CURRENT_LATCHES.PC == 0x0000) {
            RUN_BIT = FALSE;
            printf("Simulator halted\n\n");
            break;
        }
        cycle();
    }
}

/***************************************************************/
/*                                                             */
/* Procedure : go                                              */
/*                                                             */
/* Purpose   : Simulate the LC-3b until HALTed                 */
/*                                                             */
/***************************************************************/
void go() {
    if (RUN_BIT == FALSE) {
        printf("Can't simulate, Simulator is halted\n\n");
        return;
    }

    printf("Simulating...\n\n");
    while (CURRENT_LATCHES.PC != 0x0000)
        cycle();
    RUN_BIT = FALSE;
    printf("Simulator halted\n\n");
}

/***************************************************************/
/*                                                             */
/* Procedure : mdump                                           */
/*                                                             */
/* Purpose   : Dump a word-aligned region of memory to the     */
/*             output file.                                    */
/*                                                             */
/***************************************************************/
void mdump(FILE * dumpsim_file, int start, int stop) {
    int address; /* this is a byte address */

    printf("\nMemory content [0x%.4x..0x%.4x] :\n", start, stop);
    printf("-------------------------------------\n");
    for (address = (start >> 1); address <= (stop >> 1); address++)
        printf("  0x%.4x (%d) : 0x%.2x%.2x\n", address << 1, address << 1, MEMORY[address][1], MEMORY[address][0]);
    printf("\n");

    /* dump the memory contents into the dumpsim file */
    fprintf(dumpsim_file, "\nMemory content [0x%.4x..0x%.4x] :\n", start, stop);
    fprintf(dumpsim_file, "-------------------------------------\n");
    for (address = (start >> 1); address <= (stop >> 1); address++)
        fprintf(dumpsim_file, " 0x%.4x (%d) : 0x%.2x%.2x\n", address << 1, address << 1, MEMORY[address][1], MEMORY[address][0]);
    fprintf(dumpsim_file, "\n");
    fflush(dumpsim_file);
}

/***************************************************************/
/*                                                             */
/* Procedure : rdump                                           */
/*                                                             */
/* Purpose   : Dump current register and bus values to the     */
/*             output file.                                    */
/*                                                             */
/***************************************************************/
void rdump(FILE * dumpsim_file) {
    int k;

    printf("\nCurrent register/bus values :\n");
    printf("-------------------------------------\n");
    printf("Instruction Count : %d\n", INSTRUCTION_COUNT);
    printf("PC                : 0x%.4x\n", CURRENT_LATCHES.PC);
    printf("CCs: N = %d  Z = %d  P = %d\n", CURRENT_LATCHES.N, CURRENT_LATCHES.Z, CURRENT_LATCHES.P);
    printf("Registers:\n");
    for (k = 0; k < LC_3b_REGS; k++)
        printf("%d: 0x%.4x\n", k, CURRENT_LATCHES.REGS[k]);
    printf("\n");

    /* dump the state information into the dumpsim file */
    fprintf(dumpsim_file, "\nCurrent register/bus values :\n");
    fprintf(dumpsim_file, "-------------------------------------\n");
    fprintf(dumpsim_file, "Instruction Count : %d\n", INSTRUCTION_COUNT);
    fprintf(dumpsim_file, "PC                : 0x%.4x\n", CURRENT_LATCHES.PC);
    fprintf(dumpsim_file, "CCs: N = %d  Z = %d  P = %d\n", CURRENT_LATCHES.N, CURRENT_LATCHES.Z, CURRENT_LATCHES.P);
    fprintf(dumpsim_file, "Registers:\n");
    for (k = 0; k < LC_3b_REGS; k++)
        fprintf(dumpsim_file, "%d: 0x%.4x\n", k, CURRENT_LATCHES.REGS[k]);
    fprintf(dumpsim_file, "\n");
    fflush(dumpsim_file);
}

/***************************************************************/
/*                                                             */
/* Procedure : get_command                                     */
/*                                                             */
/* Purpose   : Read a command from standard input.             */
/*                                                             */
/***************************************************************/
void get_command(FILE * dumpsim_file) {
    char buffer[20];
    int start, stop, cycles;

    printf("LC-3b-SIM> ");

    scanf("%s", buffer);
    printf("\n");

    switch(buffer[0]) {
        case 'G':
        case 'g':
            go();
            break;

        case 'M':
        case 'm':
            scanf("%i %i", &start, &stop);
            mdump(dumpsim_file, start, stop);
            break;

        case '?':
            help();
            break;
        case 'Q':
        case 'q':
            printf("Bye.\n");
            exit(0);

        case 'R':
        case 'r':
            if (buffer[1] == 'd' || buffer[1] == 'D')
                rdump(dumpsim_file);
            else {
                scanf("%d", &cycles);
                run(cycles);
            }
            break;

        default:
            printf("Invalid Command\n");
            break;
    }
}

/***************************************************************/
/*                                                             */
/* Procedure : init_memory                                     */
/*                                                             */
/* Purpose   : Zero out the memory array                       */
/*                                                             */
/***************************************************************/
void init_memory() {
    int i;

    for (i=0; i < WORDS_IN_MEM; i++) {
        MEMORY[i][0] = 0;
        MEMORY[i][1] = 0;
    }
}

/**************************************************************/
/*                                                            */
/* Procedure : load_program                                   */
/*                                                            */
/* Purpose   : Load program and service routines into mem.    */
/*                                                            */
/**************************************************************/
void load_program(char *program_filename) {
    FILE * prog;
    int ii, word, program_base;

    /* Open program file. */
    prog = fopen(program_filename, "r");
    if (prog == NULL) {
        printf("Error: Can't open program file %s\n", program_filename);
        exit(-1);
    }

    /* Read in the program. */
    if (fscanf(prog, "%x\n", &word) != EOF)
        program_base = word >> 1;
    else {
        printf("Error: Program file is empty\n");
        exit(-1);
    }

    ii = 0;
    while (fscanf(prog, "%x\n", &word) != EOF) {
        /* Make sure it fits. */
        if (program_base + ii >= WORDS_IN_MEM) {
            printf("Error: Program file %s is too long to fit in memory. %x\n",
                   program_filename, ii);
            exit(-1);
        }

        /* Write the word to memory array. */
        MEMORY[program_base + ii][0] = word & 0x00FF;
        MEMORY[program_base + ii][1] = (word >> 8) & 0x00FF;
        ii++;
    }

    if (CURRENT_LATCHES.PC == 0) CURRENT_LATCHES.PC = (program_base << 1);

    printf("Read %d words from program into memory.\n\n", ii);
}

/************************************************************/
/*                                                          */
/* Procedure : initialize                                   */
/*                                                          */
/* Purpose   : Load machine language program                */
/*             and set up initial state of the machine.     */
/*                                                          */
/************************************************************/
void initialize(char *program_filename, int num_prog_files) {
    int i;

    init_memory();
    for ( i = 0; i < num_prog_files; i++ ) {
        load_program(program_filename);
        while(*program_filename++ != '\0');
    }
    CURRENT_LATCHES.Z = 1;
    NEXT_LATCHES = CURRENT_LATCHES;

    RUN_BIT = TRUE;
}

/***************************************************************/
/*                                                             */
/* Procedure : main                                            */
/*                                                             */
/***************************************************************/
int main(int argc, char *argv[]) {
    FILE * dumpsim_file;

    /* Error Checking */
    if (argc < 2) {
        printf("Error: usage: %s <program_file_1> <program_file_2> ...\n",
               argv[0]);
        exit(1);
    }

    printf("LC-3b Simulator\n\n");

    initialize(argv[1], argc - 1);

    if ( (dumpsim_file = fopen( "dumpsim", "w" )) == NULL ) {
        printf("Error: Can't open dumpsim file\n");
        exit(-1);
    }

    while (1)
        get_command(dumpsim_file);
}

/***************************************************************/
/* Do not modify the above code.
  You are allowed to use the following global variables in your
  code. These are defined above.

  MEMORY

  CURRENT_LATCHES
  NEXT_LATCHES

  You may define your own local/global variables and functions.
  You may use the functions to get at the control bits defined
  above.

  Begin your code here                                          */

/***************************************************************/

void decode(int instruction);
int get_instruction();
void getBin(char* str, int num);
void toBR(char *str, int num);
void toADD(char *str, int num);
void toLDB(char *str, int num);
void toSTB(char *str, int num);
void toJSR(char *str, int num);
void toAND(char *str, int num);
void toLDW(char *str, int num);
void toSTW(char *str, int num);
void toXOR(char *str, int num);
void toJMP(char *str, int num);
void toSHF(char *str, int num);
void toLEA(char *str, int num);
void toTRAP(char *str, int num);

void process_instruction(){
    /*  function: process_instruction
     *
     *    Process one instruction at a time
     *       -Fetch one instruction
     *       -Decode
     *       -Execute
     *       -Update NEXT_LATCHES
     */
    int instruction = get_instruction();
    decode(instruction);
}

int get_instruction() {
    int instruction = MEMORY[CURRENT_LATCHES.PC >> 1][1];
    instruction = instruction << 8;
    instruction += MEMORY[CURRENT_LATCHES.PC >> 1][0];
    return Low16bits(instruction);
}

void decode(int instruction) {
    int opcode = 0xF & (instruction >> 12);
    char sInstruction[17];
    getBin(sInstruction, instruction);
    switch(opcode) {
        case 0: toBR(sInstruction, instruction);
            break;
        case 1: toADD(sInstruction, instruction);
            break;
        case 2: toLDB(sInstruction, instruction);
            break;
        case 3: toSTB(sInstruction, instruction);
            break;
        case 4: toJSR(sInstruction, instruction);
            break;
        case 5: toAND(sInstruction, instruction);
            break;
        case 6: toLDW(sInstruction, instruction);
            break;
        case 7: toSTW(sInstruction, instruction);
            break;
        case 9: toXOR(sInstruction, instruction);
            break;
        case 12: toJMP(sInstruction, instruction);
            break;
        case 13: toSHF(sInstruction, instruction);
            break;
        case 14: toLEA(sInstruction, instruction);
            break;
        case 15: toTRAP(sInstruction, instruction);
            break;
        default: break;
    }
}

void getBin(char* str, int num) {
    char binary[17];
    binary[16] = '\0';
    int rem;
    int i;
    for(i = 15; i >= 0; i--) {
        rem=num%2;
        num/=2;
        binary[i] = (rem+'0');
    }
    strcpy(str, binary);
}

int binToDecimal(int bin) {
    int rem, i;
    int base = 1;
    int decimal = 0;
    while(bin > 0) {
        rem = bin & 1;
        decimal += rem*base;
        bin = bin >> 1;
        base *= 2;
    }
    return decimal;
}

int imm5(int num) {
    int mask = (1<<4);
    if((num & mask) == 0) return num;
    num = 0x1F & (num ^ 0x1F);
    int dec = binToDecimal(num) + 1;
    return -dec;
}

int offset6(int bin) {
    int mask = (1<<5);
    if((bin & mask) == 0) return 2*binToDecimal(bin);
    bin = bin ^ 0x2F;
    int dec = binToDecimal(bin) + 1;
    return (-dec*2);
}

int offset9(int bin) {
    int mask = (1<<8);
    if(bin < 256 || ((bin & mask) == 0)) return 2*binToDecimal(bin);
    else {
        bin = bin ^ 0x1FF;
        int dec = binToDecimal(bin) + 1;
        return (-dec*2);
    }
}

int offset11(int bin) {
    int mask = (1<<10);
    if((bin & mask) == 0) return 2*binToDecimal(bin);
    bin = bin ^ 0x7FF;
    int dec = binToDecimal(bin) + 1;
    return (-dec*2);
}

void setCC(int dR) {
    if(dR < 0 || (dR & (1 << 15))) {
        NEXT_LATCHES.N = 1;
        NEXT_LATCHES.P = NEXT_LATCHES.Z = 0;
    }
    else if(dR == 0) {
        NEXT_LATCHES.Z = 1;
        NEXT_LATCHES.N = NEXT_LATCHES.P = 0;
    }
    else {
        NEXT_LATCHES.P = 1;
        NEXT_LATCHES.N = NEXT_LATCHES.Z = 0;
    }
}

#define Register(x) ((x) & 0x07)
#define ToNum(x) (x - '0')

void toBR(char *str, int num) {
    int n = ToNum(*(str + 4)) & CURRENT_LATCHES.N;
    int z = ToNum(*(str + 5)) & CURRENT_LATCHES.Z;
    int p = ToNum(*(str + 6)) & CURRENT_LATCHES.P;
    if(n || z || p) {
        NEXT_LATCHES.PC = CURRENT_LATCHES.PC + (offset9(num & 0x1FF)) + 2;
    }
    else NEXT_LATCHES.PC = CURRENT_LATCHES.PC + 2;
}

void toADD(char *str, int num) {
    int DR = Register(num >> 9);
    int SR1 = Register(num >> 6);
    if(*(str+10) == '0') { /* DR = SR1 + SR2 */
        int SR2 = Register(num);
        NEXT_LATCHES.REGS[DR] = Low16bits(CURRENT_LATCHES.REGS[SR1] + CURRENT_LATCHES.REGS[SR2]);
    }
    else {
        int num5 = imm5(num & 0x1F);
        NEXT_LATCHES.REGS[DR] = Low16bits(CURRENT_LATCHES.REGS[SR1] + num5);
    }
    NEXT_LATCHES.PC = CURRENT_LATCHES.PC + 2;
    setCC(NEXT_LATCHES.REGS[DR]);
}

void toLDB(char *str, int num) {
    int DR = Register(num >> 9);
    int BaseR = Register(num >> 6);
    int address1 = (Low16bits(CURRENT_LATCHES.REGS[BaseR] + offset6(num & 0x2F))) >> 1;
    int address2 = ((Low16bits(CURRENT_LATCHES.REGS[BaseR] + offset6(num & 0x2F))) & 1);
    NEXT_LATCHES.REGS[DR] = Low16bits(MEMORY[address1][address2]);
    NEXT_LATCHES.PC = CURRENT_LATCHES.PC + 2;
    setCC(NEXT_LATCHES.REGS[DR]);
}

void toSTB(char *str, int num) {
    int SR = Register(num >> 9);
    int BaseR = Register(num >> 6);
    int address1 = (Low16bits(CURRENT_LATCHES.REGS[BaseR] + offset6(num & 0x2F))) >> 1;
    int address2 = ((Low16bits(CURRENT_LATCHES.REGS[BaseR] + offset6(num & 0x2F))) & 1);
    int content = Low16bits(CURRENT_LATCHES.REGS[SR]);
    MEMORY[address1][address2] = content;
    NEXT_LATCHES.PC = CURRENT_LATCHES.PC + 2;
}

void toJSR(char *str, int num) {
    NEXT_LATCHES.REGS[7] = CURRENT_LATCHES.PC + 2;
    if(*(str+4) == '0') {
        int BaseR = Register(num >> 6);
        NEXT_LATCHES.PC = CURRENT_LATCHES.REGS[BaseR];
    }
    else {
        NEXT_LATCHES.PC = CURRENT_LATCHES.PC + offset11(num & 0x7FF) + 2;
    }
}

void toAND(char *str, int num) {
    int DR = Register(num >> 9);
    int SR1 = Register(num >> 6);
    if(*(str+10) == '0') { /* DR = SR1 + SR2 */
        int SR2 = Register(num);
        NEXT_LATCHES.REGS[DR] = Low16bits(CURRENT_LATCHES.REGS[SR1] & CURRENT_LATCHES.REGS[SR2]);
    }
    else {
        NEXT_LATCHES.REGS[DR] = Low16bits(CURRENT_LATCHES.REGS[SR1] & (num & 0x1F));
    }
    NEXT_LATCHES.PC = CURRENT_LATCHES.PC + 2;
    int condition = 0;
    if(NEXT_LATCHES.REGS[DR] == 0) condition = 0;
    else if((Low16bits(NEXT_LATCHES.REGS[DR] >> 15)) == 0) condition = 1;
    else condition = -1;
    setCC(condition);
}

void toLDW(char *str, int num) {
    int DR = Register(num >> 9);
    int BaseR = Register(num >> 6);
    int address = Low16bits(CURRENT_LATCHES.REGS[BaseR] + (offset6(num & 0x2F)));
    int content = (MEMORY[address >> 1][1] << 8) + MEMORY[address >> 1][0];
    NEXT_LATCHES.REGS[DR] = Low16bits(content);
    NEXT_LATCHES.PC = CURRENT_LATCHES.PC + 2;
    setCC(content);
}

void toSTW(char *str, int num) {
    int SR = binToDecimal(Register(num >> 9));
    int BaseR = binToDecimal(Register(num >> 6));
    int address = Low16bits(CURRENT_LATCHES.REGS[BaseR] + (offset6(num & 0x2F)));
    int content = Low16bits(CURRENT_LATCHES.REGS[SR]);
    MEMORY[Low16bits(address >> 1)][0] = content & 0xF;
    MEMORY[Low16bits(address >> 1)][1] = (content >> 8) & 0xF;
    NEXT_LATCHES.PC = CURRENT_LATCHES.PC + 2;
}

void toXOR(char *str, int num) {
    int DR = Register(num >> 9);
    int SR1 = Register(num >> 6);
    if(*(str+10) == '0') {
        int SR2 = binToDecimal(Register(num));
        NEXT_LATCHES.REGS[DR] = Low16bits(CURRENT_LATCHES.REGS[SR1] ^ CURRENT_LATCHES.REGS[SR2]);
    }
    else if((num&0x2F) == 0x2F) {   /* Not operation */
        NEXT_LATCHES.REGS[DR] = Low16bits(CURRENT_LATCHES.REGS[SR1] ^ 0xFFFF);
    }
    else NEXT_LATCHES.REGS[DR] = Low16bits(CURRENT_LATCHES.REGS[SR1] ^ (num & 0x001F));
    NEXT_LATCHES.PC = CURRENT_LATCHES.PC + 2;
    int condition = 0;
    if(NEXT_LATCHES.REGS[DR] == 0) condition = 0;
    else if((Low16bits(NEXT_LATCHES.REGS[DR] >> 15)) == 0) condition = 1;
    else condition = -1;
    setCC(condition);
}

void toJMP(char *str, int num) {
    int reg = Register(num >> 6);
    NEXT_LATCHES.PC = Low16bits(CURRENT_LATCHES.REGS[reg]);
}

void toSHF(char *str, int num) {
    int DR = binToDecimal(Register(num >> 9));
    int SR = binToDecimal(Register(num >> 6));
    if(*(str+10) == '1') { /* RSHFA */
        int count = (num & 0xF);
        int bit15 = (CURRENT_LATCHES.REGS[SR] & (1 << 15));
        NEXT_LATCHES.REGS[DR] = Low16bits(CURRENT_LATCHES.REGS[SR]);
        for(; count > 0; count--) {
            NEXT_LATCHES.REGS[DR] = Low16bits(NEXT_LATCHES.REGS[DR] >> 1);
            NEXT_LATCHES.REGS[DR] += bit15;
        }
    }
    else if(*(str+11) == '1') { /* RSHFL */
        NEXT_LATCHES.REGS[DR] = Low16bits(CURRENT_LATCHES.REGS[SR] >> (num & 0xF));
    }
    else {  /* LSHF */
        NEXT_LATCHES.REGS[DR] = Low16bits(CURRENT_LATCHES.REGS[SR] << (num & 0xF));
    }
    NEXT_LATCHES.PC = CURRENT_LATCHES.PC + 2;
    int condition = 0;
    if(NEXT_LATCHES.REGS[DR] == 0) condition = 0;
    else if((Low16bits(NEXT_LATCHES.REGS[DR] >> 15)) == 0) condition = 1;
    else condition = -1;
    setCC(condition);
}

void toLEA(char *str, int num) {
    int DR = Register(num >> 9);
    NEXT_LATCHES.PC = CURRENT_LATCHES.PC + 2;
    NEXT_LATCHES.REGS[DR] = NEXT_LATCHES.PC + offset9(num & 0x1FF);
}

void toTRAP(char *str, int num) {
    NEXT_LATCHES.REGS[7] = CURRENT_LATCHES.PC + 2;
    int trapVect8 = 0xFF & num;
    NEXT_LATCHES.PC = Low16bits((MEMORY[trapVect8][1] << 8) + MEMORY[trapVect8][0]);
}
