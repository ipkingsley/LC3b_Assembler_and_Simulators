Introduction:The simulator will execute the input LC-3b program, one instruction at a time, modifying the architectural state of the LC-3b after each instruction. 

Note: The file shuffle is the output file from the assembler. Each line of this file is a hex number, starting with the prefix �0x�, followed by 4 hex characters. The first line of the file specifies the starting address for the program - e.g. 0x3000, if the original assembly program had .ORIG x3000. Each subsequent line in the file contains a hex number for each word of instruction and/or data in the program. For example, the instruction NOT R1, R6 is assembled to 1001001110111111. This instruction would be represented in the isaprogram file as 0x93BF. The pseudo op .FILL xABCD is assembled to 1010101111001101, and would be represented in the isaprogram file as 0xABCD. 

The Shell: The purpose of the shell is to provide the user with commands to control the execution of the simulator. The shell accepts one or more ISA programs as arguments and loads them into the memory image. In order to extract information from the simulator, a file named dumpsim will be created to hold information requested from the simulator. The shell supports the following 
commands: 
 
1. go � simulate the program until a HALT instruction is executed. 
2. run <n> � simulate the execution of the machine for n instructions 
3. mdump <low> <high> � dump the contents of memory, from location ?low? to location high? to the screen and the dump file 
4. rdump � dump the current instruction count, the contents of R0�R7, PC, and condition codes to the screen and the dump file. 
5. ? � print out a list of all shell commands. 
6. quit � quit the shell 